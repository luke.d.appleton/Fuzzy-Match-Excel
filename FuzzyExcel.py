import sys
import difflib
#import csv
import pandas as pd

#Import Excel file into pandas dataframe
df = pd.read_excel(sys.argv[1] , sheet_name='Sheet1')
#Name the different columns in the file and convert them to lower case/string
Master = df.Master.str.lower() #.astype(str).values.tolist()
MappedID = df.Mapped.astype(str).str.lower()
#Use difflib.get_close_matches to lookup MappedID in a giant column of known names
df['Name_r'] = MappedID.map(lambda x: (difflib.get_close_matches(x, Master, cutoff=0.8)[:1] or [None][0]))

#Write result to an excel file specified in the 2nd argument of sys
header = [ "Master", "Mapped", "Name_r"] 
df.to_excel(sys.argv[2], columns = header) # quoting=csv.QUOTE_NONE)
print(df.to_string())
